const Sentry = require('@sentry/node');

Sentry.init({
  environment: 'development-troy',
  // integrations: [captureConsoleIntegration(), debugIntegration()],
  dsn: 'http://bbc3f83f006942c7b6849ded5a8bf95d@localhost:8000/1',
  sendClientReports: false,
  tracesSampleRate: 0.0,
  autoSessionTracking: false,
  debug: true // This will enable debug mode
})

Sentry.captureMessage('Sending test from conductor')
const testError = new Error(`This is just a test from conductor`)
Sentry.captureException(testError)

